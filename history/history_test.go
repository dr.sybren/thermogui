package history

/* ***** BEGIN GPL LICENSE BLOCK *****
*
* Copyright (C) 2021 Sybren A. Stüvel.
*
* This file is part of ThermoGUI.
*
* ThermoGUI is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation, either version 3 of the License, or (at your option) any later
* version.
*
* ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
* A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with
* ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
*
* ***** END GPL LICENSE BLOCK ***** */

import (
	"os"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

const (
	historyFilename = "testdata/history.json"
)

func TestFromScratch(t *testing.T) {
	c := NewCollection()
	e := Entry{
		Timestamp:   time.Date(2021, time.November, 5, 15, 14, 13, 12, time.UTC),
		Temperature: 19.25,
		Options: Options{
			HyperMode:      true,
			DisplayEnabled: false,
		},
	}

	c.Append(&e)
	err := c.Save("tempfile.json")
	assert.Nil(t, err)
	defer os.Remove("tempfile.json")

	loaded := NewCollection()
	err = loaded.Load("tempfile.json")
	assert.Nil(t, err)

	assert.Equal(t, 1, len(loaded.all.Entries))
	assert.EqualValues(t, e, *loaded.all.Entries[0])
}

func TestFromDisk(t *testing.T) {
	wg := sync.WaitGroup{}
	wg.Add(1)
	done := func(err error) {
		defer wg.Done()
		assert.Nil(t, err)
	}

	c := NewFromDisk(historyFilename, done)
	wg.Wait()

	assert.Equal(t, 3, len(c.all.Entries))

	// Make sure the same location instance is used; pointers are compared, so
	// just having the same timezone but in a different object isn't enough.
	zone := c.all.Entries[0].Timestamp.Location()
	assert.EqualValues(t,
		Entry{
			Timestamp:   time.Date(2021, time.November, 5, 22, 12, 13, 0, zone),
			Temperature: 20.18,
			Options: Options{
				HyperMode:      false,
				DisplayEnabled: false,
			},
		},
		*c.all.Entries[0],
	)

	assert.EqualValues(t,
		Entry{
			Timestamp:   time.Date(2021, time.November, 5, 22, 12, 17, 0, zone),
			Temperature: 20.19,
			Options: Options{
				HyperMode:      true,
				DisplayEnabled: false,
			},
		},
		*c.all.Entries[1],
	)
	assert.EqualValues(t,
		Entry{
			Timestamp:   time.Date(2021, time.November, 5, 22, 12, 21, 0, zone),
			Temperature: 20.20,
			Options: Options{
				HyperMode:      false,
				DisplayEnabled: true,
			},
		},
		*c.all.Entries[2],
	)
}

func TestLoadNonExisting(t *testing.T) {
	wg := sync.WaitGroup{}
	wg.Add(1)
	done := func(err error) {
		defer wg.Done()
		assert.Nil(t, err)
	}

	c := NewFromDisk("nonexistant.json", done)
	wg.Wait()

	assert.Equal(t, 0, len(c.all.Entries))
}
