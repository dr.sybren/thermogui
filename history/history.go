package history

/* ***** BEGIN GPL LICENSE BLOCK *****
*
* Copyright (C) 2021 Sybren A. Stüvel.
*
* This file is part of ThermoGUI.
*
* ThermoGUI is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation, either version 3 of the License, or (at your option) any later
* version.
*
* ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
* A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with
* ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
*
* ***** END GPL LICENSE BLOCK ***** */

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"sync"
)

type Collection struct {
	all   Document
	mutex *sync.Mutex
}

func NewCollection() Collection {
	return Collection{
		mutex: new(sync.Mutex),
	}
}

func NewFromDisk(path string, done func(error)) *Collection {
	c := NewCollection()
	go func() {
		err := c.Load(path)
		if os.IsNotExist(err) {
			done(nil)
		} else {
			done(err)
		}
	}()
	return &c
}

func (c *Collection) Append(entry *Entry) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	c.all.Entries = append(c.all.Entries, entry)
}

func (c *Collection) Load(path string) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	fromDisk := Document{}
	err = json.Unmarshal(data, &fromDisk)
	if err != nil {
		return err
	}

	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.all.Entries = append(fromDisk.Entries, c.all.Entries...)
	return nil
}

func (c *Collection) Save(path string) error {
	data, err := json.MarshalIndent(c.all, "", "  ")
	if err != nil {
		return err
	}

	return ioutil.WriteFile(path, data, 0644)
}

func (c *Collection) IsEmpty() bool {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	return len(c.all.Entries) == 0
}
