package application

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"time"

	"gitlab.com/dr.sybren/thermogui/history"
)

func (t *ThermoGUI) pollTemperature() {
	// The channel will close when the port closes.
	stateChan := t.device.Poll(t.portCtx, t.onPollError)
	for state := range stateChan {
		now := time.Now()

		t.window.SetTemperature(state.Temperature)
		t.window.SetState(state.HyperMode, state.DisplayEnabled)

		t.history.Append(&history.Entry{
			Timestamp:   now,
			Temperature: state.Temperature,
			Options: history.Options{
				HyperMode:      state.HyperMode,
				DisplayEnabled: state.DisplayEnabled,
			},
		})
	}
}

func (t *ThermoGUI) onPollError(err error) {
	if err == nil {
		panic("err is nil")
	}

	if t.device.IsConnected() {
		t.onDisconnectButton()
	}
	t.window.ShowError(err)
}

func (t *ThermoGUI) pollStatus() {
	for {
		select {
		case <-t.portCtx.Done():
			return
		case <-time.After(2 * time.Second):
			t.sendStatusRequest()
		}
	}
}

func (t *ThermoGUI) sendStatusRequest() {
	err := t.device.SendStatusRequest()
	t.window.ShowError(err)
	// TODO: disconnect from the port?
}
