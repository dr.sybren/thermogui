package application

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"context"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/dialog"

	"gitlab.com/dr.sybren/thermogui/gui"
	"gitlab.com/dr.sybren/thermogui/history"
	"gitlab.com/dr.sybren/thermogui/thermo"
)

type ThermoGUI struct {
	fyneApp fyne.App
	window  *gui.MainWindow

	appCtx           context.Context
	appCtxCancelFunc context.CancelFunc

	device            *thermo.Device
	portCtx           context.Context
	portCtxCancelFunc context.CancelFunc

	history *history.Collection
}

func NewApp() *ThermoGUI {
	fyneApp := app.NewWithID("eu.stuvel.thermogui")
	fyneApp.Settings().SetTheme(&gui.ThermoTheme{})

	serialPorts, err := thermo.GetPortsList()
	if err != nil {
		dialog.ShowError(err, nil)
		return nil
	}

	mainWin := gui.NewWindow(fyneApp, serialPorts)
	mainWin.SetSerialPort(fyneApp.Preferences().String("serialPort"))

	ctx, cancelFunc := context.WithCancel(context.Background())

	thermoGUI := &ThermoGUI{
		fyneApp: fyneApp,
		window:  mainWin,

		appCtx:           ctx,
		appCtxCancelFunc: cancelFunc,
	}

	mainWin.OnConnect = thermoGUI.onConnectButton
	mainWin.OnDisconnect = thermoGUI.onDisconnectButton
	mainWin.OnStateChangeRequested = thermoGUI.onStateChangeRequested

	return thermoGUI
}
