package thermo

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"encoding/json"
	"fmt"
	"strings"
)

func updateTemperature(state *State, line string) error {
	temperature, err := parseTemperature(line)
	if err == errNotTemperature {
		// This is an expected situation, not all lines received will be temps.
		return nil
	}
	if err != nil {
		return fmt.Errorf("error parsing temperature: %w", err)
	}

	state.Temperature = temperature
	state.hasSeenTemp = true
	return nil
}

func parseTemperature(line string) (float32, error) {
	if len(line) > 0 && line[0] == '{' {
		return tempFromJSON(line)
	}

	prefixIndex := strings.LastIndex(line, "Temperature: ")
	if prefixIndex == -1 {
		return 0, errNotTemperature
	}

	var temp float32
	_, err := fmt.Sscanf(line[prefixIndex:], "Temperature: %f", &temp)
	if err != nil {
		return 0, fmt.Errorf("input %#v: %v", line, err)
	}

	return temp, nil
}

/* This is from the ORK LEDs v2 project. */
func tempFromJSON(line string) (float32, error) {
	data := struct {
		ADC struct {
			Green int16 `json:"green"`
			White int16 `json:"white"`
		} `json:"adc"`
		VRef float32 `json:"vref"`
		Temp float32 `json:"temp"`
	}{}

	fmt.Printf("JSON: %v\n", line)
	if err := json.Unmarshal([]byte(line), &data); err != nil {
		fmt.Printf("    error: %v\n", err)
		return 0, err
	}
	return data.Temp, nil
}
