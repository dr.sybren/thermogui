package application

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/dr.sybren/thermogui/gui"
	"gitlab.com/dr.sybren/thermogui/thermo"
)

func (t *ThermoGUI) onConnectButton() {
	t.window.SetConnectionStatus(gui.ConnectionStatusConnecting)
	t.window.SetStatusMessage(fmt.Sprintf("Connecting to %v", t.window.GetSerialPort()))

	connectCtx, cancel := context.WithDeadline(t.appCtx, time.Now().Add(5*time.Second))
	defer cancel()

	// Cancelled when the port closes.
	t.portCtx, t.portCtxCancelFunc = context.WithCancel(t.appCtx)

	t.device = thermo.NewDevice()
	err := t.device.Open(connectCtx, t.window.GetSerialPort())
	t.window.ShowError(err)
	if err != nil {
		fmt.Println("failed to connect:", err)
		t.window.SetConnectionStatus(gui.ConnectionStatusDisconnected)
		return
	}

	// Respond to temperature reports.
	go t.pollTemperature()

	// Occasionally ask for a status report.
	go t.pollStatus()

	t.window.SetConnectionStatus(gui.ConnectionStatusConnected)
	t.window.SetStatusMessage("Connected")
	t.fyneApp.Preferences().SetString("serialPort", t.window.GetSerialPort())
}

func (t *ThermoGUI) onDisconnectButton() {
	t.window.SetConnectionStatus(gui.ConnectionStatusDisconnecting)
	t.window.SetStatusMessage("Disconnecting")

	disconnCtx, cancel := context.WithDeadline(t.appCtx, time.Now().Add(5*time.Second))
	defer cancel()

	err := t.device.Close(disconnCtx)
	t.window.ShowError(err)
	if err != nil {
		fmt.Println("failed to disconnect:", err)
		t.window.SetConnectionStatus(gui.ConnectionStatusConnected)
		return
	}

	t.portCtxCancelFunc()
	t.window.SetConnectionStatus(gui.ConnectionStatusDisconnected)
	t.window.SetStatusMessage("Disconnected")
	t.window.HideTemperature()
}

func (t *ThermoGUI) onStateChangeRequested(hyperMode, displayEnabled bool) {
	// The GUI can send us status changes, but they should only be handled when actually connected.
	if t.device == nil || !t.device.IsConnected() {
		return
	}
	t.device.ChangeStatus(hyperMode, displayEnabled)
	t.device.SendStatusRequest()
}

func (t *ThermoGUI) ShowAndRun() {
	t.window.ShowAndRun()
	t.appCtxCancelFunc()
}
