package gui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"fmt"

	fyne "fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"

	"gitlab.com/dr.sybren/thermogui/appinfo"
)

type ConnectionStatus int

const (
	ConnectionStatusDisconnected ConnectionStatus = iota
	ConnectionStatusConnecting
	ConnectionStatusConnected
	ConnectionStatusDisconnecting
)

type MainWindow struct {
	win fyne.Window

	normalStatusMessage string
	statusErr           error

	// Input & output sections
	serialPort     binding.String
	temperature    binding.String
	statusMessage  binding.String
	hyperMode      binding.Bool
	displayEnabled binding.Bool

	serialPortEntry  *widget.Select
	connectionStatus ConnectionStatus
	connectButton    *widget.Button
	stateContainer   *fyne.Container

	// Callbacks to be provided by the app.
	OnConnect              func()
	OnDisconnect           func()
	OnStateChangeRequested func(hyperMode bool, displayEnabled bool)
}

type WindowCreator interface {
	NewWindow(title string) fyne.Window
}

func NewWindow(wc WindowCreator, serialPorts []string) *MainWindow {
	fyneWindow := wc.NewWindow(appinfo.FormattedApplicationInfo())
	fyneWindow.Resize(fyne.NewSize(300, 0))

	mw := MainWindow{
		win: fyneWindow,

		serialPort:    binding.NewString(),
		temperature:   binding.NewString(),
		statusMessage: binding.NewString(),

		hyperMode:      binding.NewBool(),
		displayEnabled: binding.NewBool(),

		OnConnect:              nil,
		OnDisconnect:           nil,
		OnStateChangeRequested: nil,
	}

	// Serial port selector.

	mw.serialPortEntry = widget.NewSelect(serialPorts, mw.onSerialPortChanged)
	mw.serialPortEntry.PlaceHolder = "COM4 or /dev/ttyACM0 or something"

	mw.connectButton = widget.NewButton("Connect", mw.onConnectButtonClicked)

	mw.stateContainer = container.NewVBox(
		widget.NewCheckWithData("Hyper Mode", mw.hyperMode),
		widget.NewCheckWithData("Display Enabled", mw.displayEnabled),
	)
	mw.hyperMode.AddListener(binding.NewDataListener(mw.onStateCheckboxChanged))
	mw.displayEnabled.AddListener(binding.NewDataListener(mw.onStateCheckboxChanged))

	formContainer := container.New(
		layout.NewFormLayout(),
		widget.NewLabel("Port:"), mw.serialPortEntry,
		widget.NewLabel("Temperature:"), widget.NewLabelWithData(mw.temperature),
		widget.NewLabel("State:"), mw.stateContainer,
	)

	buttonBar := container.NewHBox(
		widget.NewLabelWithData(mw.statusMessage),
		layout.NewSpacer(),
		mw.connectButton,
	)

	fyneWindow.SetContent(container.NewVBox(
		formContainer,
		buttonBar,
	))

	mw.SetConnectionStatus(ConnectionStatusDisconnected)

	return &mw
}

func (mw *MainWindow) ShowAndRun() {
	mw.win.ShowAndRun()
}

func (mw *MainWindow) onSerialPortChanged(port string) {
	mw.serialPort.Set(port)
}

func (mw *MainWindow) SetSerialPort(port string) {
	mw.serialPortEntry.SetSelected(port)
}

func (mw *MainWindow) GetSerialPort() string {
	port, _ := mw.serialPort.Get()
	return port
}

func (mw *MainWindow) SetStatusMessage(message string) {
	mw.normalStatusMessage = message

	// Normal status messages shouldn't clear out the error status.
	if mw.statusErr == nil {
		mw.statusMessage.Set(message)
	}
}

func (mw *MainWindow) ShowError(err error) {
	if err == nil {
		// Error condition has gone away, return to the normal status message.
		mw.statusErr = nil
		mw.statusMessage.Set(mw.normalStatusMessage)
		return
	}

	mw.statusErr = err
	mw.statusMessage.Set(err.Error())
}

func (mw *MainWindow) SetConnectionStatus(status ConnectionStatus) {
	mw.connectionStatus = status
	switch status {
	case ConnectionStatusDisconnected:
		mw.connectButton.SetText("Connect")
		mw.connectButton.Enable()
		disableContainer(mw.stateContainer)
	case ConnectionStatusConnecting:
		mw.connectButton.SetText("Connecting...")
		mw.connectButton.Disable()
		disableContainer(mw.stateContainer)
	case ConnectionStatusConnected:
		mw.connectButton.SetText("Disconnect")
		mw.connectButton.Enable()
		enableContainer(mw.stateContainer)
	case ConnectionStatusDisconnecting:
		mw.connectButton.SetText("Disconnecting...")
		mw.connectButton.Disable()
		disableContainer(mw.stateContainer)
	}
}

func (mw *MainWindow) onConnectButtonClicked() {
	switch mw.connectionStatus {
	case ConnectionStatusDisconnected:
		mw.OnConnect()
	case ConnectionStatusConnected:
		mw.OnDisconnect()
	}
}

func (mw *MainWindow) SetTemperature(temperature float32) {
	formatted := fmt.Sprintf("%.2f °C", temperature)
	mw.temperature.Set(formatted)

	title := fmt.Sprintf("%s %s", formatted, appinfo.FormattedApplicationInfo())
	mw.win.SetTitle(title)
}

func (mw *MainWindow) HideTemperature() {
	mw.temperature.Set("")
	mw.win.SetTitle(appinfo.FormattedApplicationInfo())
}

func (mw *MainWindow) SetState(hyperMode bool, displayEnabled bool) {
	mw.hyperMode.Set(hyperMode)
	mw.displayEnabled.Set(displayEnabled)
}

func (mw *MainWindow) onStateCheckboxChanged() {
	if mw.OnStateChangeRequested == nil {
		return
	}
	hyper, _ := mw.hyperMode.Get()
	display, _ := mw.displayEnabled.Get()
	mw.OnStateChangeRequested(hyper, display)
}
