package thermo

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"context"
	"errors"
	"fmt"
	"sort"

	"go.bug.st/serial"
)

var serialMode = serial.Mode{
	BaudRate: 38400,
	DataBits: 8,
	Parity:   serial.NoParity,
	StopBits: serial.OneStopBit,
}

func LogSerialPorts() error {
	ports, err := serial.GetPortsList()
	if err != nil {
		return fmt.Errorf("failed to get ports list: %v", err)
	}
	if len(ports) == 0 {
		return fmt.Errorf("no serial ports found")
	}
	for _, port := range ports {
		fmt.Printf("Found port: %v\n", port)
	}
	return nil
}

func GetPortsList() ([]string, error) {
	ports, err := serial.GetPortsList()
	if err != nil {
		return nil, fmt.Errorf("failed to get ports list: %v", err)
	}
	if len(ports) == 0 {
		return nil, errors.New("no serial ports available")
	}

	sort.Strings(ports)
	return ports, nil
}

func (d *Device) Open(ctx context.Context, devicePath string) error {
	port, err := openSerial(ctx, devicePath)
	if err != nil {
		d.Port = nil
		return err
	}
	d.Port = port
	return nil
}

func (d *Device) Close(ctx context.Context) error {
	err := closeSerial(ctx, d.Port)
	if err != nil {
		return err
	}
	d.Port = nil
	return nil
}

func (d *Device) IsConnected() bool {
	return d.Port != nil
}

func openSerial(ctx context.Context, devicePath string) (serial.Port, error) {
	openDone := make(chan struct{})

	var port serial.Port
	var err error

	go func() {
		port, err = serial.Open(devicePath, &serialMode)
		close(openDone)
	}()

	select {
	case <-ctx.Done():
		return nil, fmt.Errorf("timeout opening %v", devicePath)
	case <-openDone:
	}

	if err != nil {
		return nil, fmt.Errorf("failed to open port: %w", err)
	}
	return port, nil
}

func closeSerial(ctx context.Context, port serial.Port) error {
	closeDone := make(chan struct{})

	var err error

	go func() {
		err = port.Close()
		close(closeDone)
	}()

	select {
	case <-ctx.Done():
		return errors.New("context canceled")
	case <-closeDone:
	}

	if err != nil {
		return fmt.Errorf("error closing port: %v", err)
	}
	return nil
}
