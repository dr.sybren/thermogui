package thermo

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseTemperatureInvalidLine(t *testing.T) {
	temp, err := parseTemperature("Hello there!")
	assert.Equal(t, errNotTemperature, err)
	assert.Equal(t, float32(0), temp)
}
func TestParseTemperatureHappy(t *testing.T) {
	temp, err := parseTemperature("Temperature: 23.45")
	assert.Nil(t, err)
	assert.Equal(t, float32(23.45), temp)
}

func TestParseTemperatureANSICodes(t *testing.T) {
	temp, err := parseTemperature("\033[2JTemperature: 23.45")
	assert.Nil(t, err)
	assert.Equal(t, float32(23.45), temp)
}
