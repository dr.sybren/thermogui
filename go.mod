module gitlab.com/dr.sybren/thermogui

go 1.16

require (
	fyne.io/fyne/v2 v2.1.1
	github.com/josephspurrier/goversioninfo v1.3.0
	github.com/stretchr/testify v1.6.1
	go.bug.st/serial v1.3.3
)
