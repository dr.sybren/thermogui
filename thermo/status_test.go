package thermo

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseStatusInvalidLine(t *testing.T) {
	state := State{}
	err := updateStatus(&state, "Hello there!")
	assert.Equal(t, errNotStatus, err)
	assert.Equal(t, false, state.DisplayEnabled)
	assert.Equal(t, false, state.HyperMode)
}

func TestParseStatusFull(t *testing.T) {
	state := State{}
	err := updateStatus(&state, "Status: H1D1")
	assert.Nil(t, err)
	assert.Equal(t, true, state.DisplayEnabled)
	assert.Equal(t, true, state.HyperMode)
}

func TestParseStatusOtherOrder(t *testing.T) {
	state := State{}
	err := updateStatus(&state, "Status: D1H1")
	assert.Nil(t, err)
	assert.Equal(t, true, state.DisplayEnabled)
	assert.Equal(t, true, state.HyperMode)
}

func TestParseStatusPartial(t *testing.T) {
	state := State{}
	err := updateStatus(&state, "Status: D1")
	assert.Nil(t, err)
	assert.Equal(t, true, state.DisplayEnabled)
	assert.Equal(t, false, state.HyperMode)

	err = updateStatus(&state, "Status: H1")
	assert.Nil(t, err)
	assert.Equal(t, true, state.DisplayEnabled)
	assert.Equal(t, true, state.HyperMode)
}
