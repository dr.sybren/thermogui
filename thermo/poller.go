package thermo

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"bufio"
	"context"
	"fmt"
	"strings"

	"go.bug.st/serial"
)

var (
	errNotTemperature = fmt.Errorf("not a temperature line")
	errNotStatus      = fmt.Errorf("not a status line")
)

type State struct {
	Temperature float32
	hasSeenTemp bool

	HyperMode      bool
	DisplayEnabled bool
}

type Device struct {
	Port         serial.Port
	CurrentState State
}

func NewDevice() *Device {
	return &Device{
		Port:         nil,
		CurrentState: State{},
	}
}

func (d *Device) Poll(ctx context.Context, onError func(error)) <-chan State {
	channel := make(chan State)
	scanner := bufio.NewScanner(d.Port)

	go func() {
		for scanner.Scan() {
			line := strings.TrimSpace(scanner.Text())

			updateTemperature(&d.CurrentState, line)
			updateStatus(&d.CurrentState, line)

			if d.CurrentState.hasSeenTemp {
				channel <- d.CurrentState
			}
		}

		// Only report errors when the context isn't done yet (i.e when the port is
		// still expected to work).
		select {
		case <-ctx.Done():
		default:
			if err := scanner.Err(); err != nil {
				onError(err)
			}
		}

		close(channel)
	}()

	return channel
}
