package main

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"flag"
	"fmt"
	"time"

	"gitlab.com/dr.sybren/thermogui/appinfo"
	"gitlab.com/dr.sybren/thermogui/application"
)

var cliArgs struct {
	version bool
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.version, "version", false, "Show version number and quit.")
	flag.Parse()
}

func main() {
	parseCliArgs()
	if cliArgs.version {
		fmt.Printf("%s %s\n", appinfo.ApplicationName, appinfo.ApplicationVersion)
		return
	}

	a := application.NewApp()
	go a.LoadHistory()
	a.ShowAndRun()
	a.SaveHistory()

	// TODO: actually have a good way to wait for background processes to get nuked.
	time.Sleep(250 * time.Millisecond)
}
