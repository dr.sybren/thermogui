package thermo

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"fmt"
	"strings"
)

func (d *Device) SendStatusRequest() error {
	_, err := d.Port.Write([]byte("s"))
	return err
}

func (d *Device) sendCommand(cmd string) error {
	_, err := d.Port.Write([]byte(cmd))
	return err
}

func (d *Device) ChangeStatus(hyperMode bool, displayEnabled bool) error {
	if d.CurrentState.HyperMode != hyperMode {
		if err := d.sendCommand("h"); err != nil {
			return fmt.Errorf("error toggling hyper mode: %v", err)
		}
	}

	if d.CurrentState.DisplayEnabled != displayEnabled {
		if err := d.sendCommand("d"); err != nil {
			return fmt.Errorf("error toggling display: %v", err)
		}
	}

	return nil
}

func updateStatus(state *State, line string) error {
	prefixIndex := strings.LastIndex(line, "Status: ")
	if prefixIndex == -1 {
		return errNotStatus
	}

	statusString := line[prefixIndex:]
	for i := 0; i < len(statusString)-1; i += 2 {
		field := statusString[i]
		switch field {
		case 'H':
			state.HyperMode = statusString[i+1] != '0'
		case 'D':
			state.DisplayEnabled = statusString[i+1] != '0'
		}
	}

	return nil
}
