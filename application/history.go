package application

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of ThermoGUI.
 *
 * ThermoGUI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * ThermoGUI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ThermoGUI.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"os"

	"fyne.io/fyne/v2/dialog"
	"gitlab.com/dr.sybren/thermogui/history"
)

const historyFilename = "history.json"

func (t *ThermoGUI) LoadHistory() {
	t.history = history.NewFromDisk(historyFilename, func(err error) {
		if err == nil {
			return
		}

		t.window.ShowError(err)
	})
}

func (t *ThermoGUI) SaveHistory() {
	if t.history == nil {
		return
	}

	if t.history.IsEmpty() {
		os.Remove(historyFilename)
		return
	}

	if err := t.history.Save(historyFilename); err != nil {
		dialog.ShowError(err, nil)
	}
}
